package ru.arubtsova.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.ICommandRepository;
import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.api.repository.IUserRepository;
import ru.arubtsova.tm.api.service.*;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.exception.system.UnknownCommandException;
import ru.arubtsova.tm.repository.CommandRepository;
import ru.arubtsova.tm.repository.ProjectRepository;
import ru.arubtsova.tm.repository.TaskRepository;
import ru.arubtsova.tm.repository.UserRepository;
import ru.arubtsova.tm.service.*;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull final AbstractCommand command : commandList) {
            registry(command);
        }
    }

    private void initData() {
        @NotNull String testId = userService.create("test", "test", "test@test.ru").getId();
        projectService.add(testId, "Project N1", "123");
        taskService.add(testId, "Task1 4 P1", "for today");
        taskService.add(testId, "Task2 4 P1", "for tomorrow");
        taskService.add(testId, "Task3 4 P1", "for 20.02");
        @NotNull String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
        projectService.add(adminId, "Project MAIN", "123");
        taskService.add(adminId, "Task1 4 MAIN", "planned");
        taskService.add(adminId, "Task2 4 MAIN", "-");
        taskService.add(adminId, "Task3 4 MAIN", "obligatory");
    }

    {
        initCommands(commandService.getCommandList());
    }

    public void run(@Nullable final String... args) {
        logService.debug("Application started");
        logService.info("*** WELCOME TO TASK-MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true) {
            System.out.println("Enter Command:");
            try {
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("Ok");
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println("Fail");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = arguments.get(arg);
        if (!Optional.ofNullable(command).isPresent()) return;
        command.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent() || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRole(roles);
        command.execute();
    }

}
