package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperCompany();

}
