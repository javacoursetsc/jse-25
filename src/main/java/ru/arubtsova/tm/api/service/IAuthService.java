package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.model.User;

import java.util.Optional;

public interface IAuthService {

    @NotNull
    Optional<User> getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRole(@Nullable Role... roles);

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void register(@NotNull String login, @NotNull String password, @NotNull String email);

}
