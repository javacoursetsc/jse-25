package ru.arubtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.api.service.IProjectTaskService;
import ru.arubtsova.tm.exception.empty.EmptyIdException;
import ru.arubtsova.tm.exception.empty.EmptyUserIdException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.bindTaskToProject(userId, taskId, projectId);
    }

    @Nullable
    @Override
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        return taskRepository.unbindTaskFromProject(userId, taskId);
    }

    @Nullable
    @Override
    public Project removeProjectWithTasksById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

}
