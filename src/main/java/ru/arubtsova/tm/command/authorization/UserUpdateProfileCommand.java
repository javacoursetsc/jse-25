package ru.arubtsova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.exception.entity.UserNotFoundException;
import ru.arubtsova.tm.model.User;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "update-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "add your name to your profile.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Update Profile:");
        System.out.println("Enter First Name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Last Name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final Optional<User> user = serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("Profile was successfully updated");
    }

}
