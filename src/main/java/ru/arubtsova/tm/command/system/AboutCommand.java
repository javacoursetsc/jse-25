package ru.arubtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("About:");
        System.out.println("Name: " + serviceLocator.getPropertyService().getDeveloperName());
        System.out.println("E-mail: " + serviceLocator.getPropertyService().getDeveloperEmail());
        System.out.println("Company: " + serviceLocator.getPropertyService().getDeveloperCompany());
    }

}
